#!/bin/bash

set -euxo pipefail

pushd aws

terraform init
S3_URI="$( terraform output --raw s3_uri )"

popd

aws s3 sync reprepro/ "$S3_URI"/conf/ --delete --no-progress

