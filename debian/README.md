
# debian/

Put the two scripts in the `debian/` directory of the debian packaging branch of a project
that builds debian packages.

Integrate the example yml to the project's `.gitlab-ci.yml`.

Make sure the following CI variables are defined in gitlab:

* `AWS_DEFAULT_REGION` -- e.g. `"us-east-1"`
* `AWS_ACCESS_KEY_ID` -- credentials for the gitlab user created by terraform
* `AWS_SECRET_ACCESS_KEY` -- credentials for the gitlab user created by terraform
* `S3_URI` -- URL for the S3 bucket created by terraform
* `SIGNING_KEY_ARN` -- ARN for the secret created by terraform

The publish CI script does *not* handle a password protected signing key. You must ensure
that the private key in the secrets manager is unprotected.

