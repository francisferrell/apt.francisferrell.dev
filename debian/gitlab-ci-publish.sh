#!/bin/bash
#
# required env vars:
#   S3_URI
#   SIGNING_KEY_ARN

set -euxo pipefail

CLOUDFRONT_ID=''
DISTRO="${DISTRO:-stable}"

export GNUPGHOME="$( mktemp --directory --tmpdir=. gpg.XXXXXXXXXX )"
export REPREPRO_BASE_DIR="$( mktemp --directory --tmpdir=. repository.XXXXXXXXXX )"



function get-infrastructure() {
    local comment="${S3_URI#s3://}"
    CLOUDFRONT_ID="$( aws cloudfront list-distributions | jq -r ".DistributionList.Items[] | select( .Comment == \"${comment}\" ) | .Id" )"

    aws secretsmanager get-secret-value --secret-id "$SIGNING_KEY_ARN" | jq -r .SecretString | gpg --import
}



function publish() {
    aws s3 sync "$S3_URI" "$REPREPRO_BASE_DIR" --delete --no-progress
    reprepro includedeb "$DISTRO" package/*.deb
    aws s3 sync "$REPREPRO_BASE_DIR" "$S3_URI" --delete --no-progress | tee aws-s3-sync.log

    if [[ -z $CLOUDFRONT_ID ]] ; then
        echo "skipping cloudfront invalidation: no distribution ID found"
    elif [[ $( wc -l <aws-s3-sync.log ) -eq 0 ]] ; then
        echo "skipping cloudfront invalidation: nothing changed"
    else
        local id="$( aws cloudfront create-invalidation --distribution-id "$CLOUDFRONT_ID" --paths '/*' | jq -r '.Invalidation.Id' )"
        aws cloudfront wait invalidation-completed --distribution-id "$CLOUDFRONT_ID" --id "$id"
    fi
}



function cleaup() {
    if [[ -n ${GNUPGHOME:-} ]] ; then
        rm -rf "$GNUPGHOME"
    fi

    if [[ -n "${REPREPRO_BASE_DIR:-}" ]] ; then
        rm -rf "$REPREPRO_BASE_DIR"
    fi
}

trap cleaup INT TERM EXIT



get-infrastructure
publish

