#!/bin/bash

set -euxo pipefail

apt-get update

mk-build-deps --install \
    --tool='apt-get -y -q -o Debug::pkgProblemResolver=yes --no-install-recommends' \
    --remove debian/control

gbp buildpackage -us -uc --git-ignore-branch --git-ignore-new

mv ../build-deb ./package

