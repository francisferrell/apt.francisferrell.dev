
variable "name" {
  type = string
  description = "the name of the signing key"
}

variable "tags" {
  type = map(string)
  description = "tags to be applied to every resource"
}



output "secret_arn" {
  description = "the ARN of the secret"
  value = aws_secretsmanager_secret.signing_key.arn
}

output "read_policy_arn" {
  description = "the IAM policy ARN granting read access to the secret"
  value = aws_iam_policy.read_policy.arn
}



resource "aws_secretsmanager_secret" "signing_key" {
  name = "${var.name}-signing-key"
  description = "signing key for ${var.name}"
  tags = var.tags
}



data "aws_iam_policy_document" "read_access" {
  statement {
    actions = [
      "secretsmanager:GetSecretValue",
    ]

    resources = [
      aws_secretsmanager_secret.signing_key.arn
    ]
  }
}

resource "aws_iam_policy" "read_policy" {
  name = "${var.name}-signing-key-read-access"
  description = "grants access to read the secret signing key for ${var.name}"
  policy = data.aws_iam_policy_document.read_access.json
  tags = var.tags
}

