
# apt.francisferrell.dev

AWS infrastructure, gitlab CI, and helper scripts to manage my apt package repository

See `aws/` for how terraform is used to create the AWS infrastructure.

See `reprepro/` and `.gitlab-ci.yml` for how the repository is initially configured.

See `debian/` for how another gitlab project can build and publish debian packages to
the repository.

See `scripts/` for how one-off operations, not using gitlab CI, on the repository can be done.

