#!/bin/bash

function usage() {
    cat 1>&2 <<EOF
$0

Sign the existing repository with the current signing key
EOF
}

if [[ $1 == '-h' ]] ; then
    usage
    exit 1
fi

set -euo pipefail

S3_URI=''
CLOUDFRONT_ID=''
SIGNING_KEY_ARN=''

export GNUPGHOME="$( mktemp --directory --tmpdir=. gpg.XXXXXXXXXX )"
export REPREPRO_BASE_DIR="$( mktemp --directory --tmpdir=. repository.XXXXXXXXXX )"



function get-infrastructure() {
    local dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
    pushd "$dir"/../aws

    terraform init
    S3_URI="$( terraform output --raw s3_uri )"
    CLOUDFRONT_ID="$( terraform output --raw cloudfront_id )"
    SIGNING_KEY_ARN="$( terraform output --raw signing_key_arn )"

    popd

    aws secretsmanager get-secret-value --secret-id "$SIGNING_KEY_ARN" | jq -r .SecretString | gpg --import
}



function sign() {
    aws s3 sync "$S3_URI" "$REPREPRO_BASE_DIR" --delete --no-progress
    reprepro export
    aws s3 sync "$REPREPRO_BASE_DIR" "$S3_URI" --delete --no-progress | tee s3-sync.log

    if [[ -z $CLOUDFRONT_ID ]] ; then
        echo "skipping cloudfront invalidation: no distribution ID found"
    elif [[ $( wc -l <s3-sync.log ) -eq 0 ]] ; then
        echo "skipping cloudfront invalidation: nothing changed"
    else
        local id="$( aws cloudfront create-invalidation --distribution-id "$CLOUDFRONT_ID" --paths '/*' | jq -r '.Invalidation.Id' )"
        aws cloudfront wait invalidation-completed --distribution-id "$CLOUDFRONT_ID" --id "$id"
    fi
}



function cleaup() {
    if [[ -n ${GNUPGHOME:-} ]] ; then
        rm -rf "$GNUPGHOME"
    fi

    if [[ -n "${REPREPRO_BASE_DIR:-}" ]] ; then
        rm -rf "$REPREPRO_BASE_DIR"
    fi
}

trap cleaup INT TERM EXIT


get-infrastructure
sign

