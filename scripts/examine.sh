#!/bin/bash

set -euo pipefail

S3_URI=''
export REPREPRO_BASE_DIR="$( mktemp --directory --tmpdir=. repository.XXXXXXXXXX )"



function get-infrastructure() {
    local dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
    pushd "$dir"/../aws

    terraform init
    S3_URI="$( terraform output --raw s3_uri )"

    popd
}



function examine() {
    aws s3 sync "$S3_URI" "$REPREPRO_BASE_DIR" --delete --no-progress
    cat <<EOF

The repository is ready to examine in ${REPREPRO_BASE_DIR}

For convenience, run:

    export REPREPRO_BASE_DIR="$( readlink -m "$REPREPRO_BASE_DIR" )"
EOF
}


get-infrastructure
examine

